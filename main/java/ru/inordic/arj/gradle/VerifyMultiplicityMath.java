package ru.inordic.arj.gradle;

public interface VerifyMultiplicityMath {

    boolean verifyTwo(int[] arrayInt);

    boolean verifyThree(int[] arrayInt);

    boolean verifyFour(int[] arrayInt);

    boolean verifyFive(int[] arrayInt);

    boolean verifyNine(int[] arrayInt);

    boolean verifyEleven(int[] arrayInt);
}

