package ru.inordic.arj.gradle;

public class VerifyMultiplicityLogic implements VerifyMultiplicityMath {
    public void printResult(int[] arrayInt) {
        System.out.println("Введенное число кратно 2: " + verifyTwo(arrayInt));
        System.out.println("Введенное число кратно 3: " + verifyThree(arrayInt));
        System.out.println("Введенное число кратно 4: " + verifyFour(arrayInt));
        System.out.println("Введенное число кратно 5: " + verifyFive(arrayInt));
        System.out.println("Введенное число кратно 9: " + verifyNine(arrayInt));
        System.out.println("Введенное число кратно 11: " + verifyEleven(arrayInt));
    }

    @Override
    public boolean verifyTwo(int[] arrayInt) {
        //Если число оканчивается на 0, 2, 4, 6, 8
        int lastElement = Math.abs(arrayInt[arrayInt.length - 1]);
        if (lastElement == 0 || lastElement == 2 || lastElement == 4 || lastElement == 6 || lastElement == 8)
            return true;
        else return false;
    }

    @Override
    public boolean verifyThree(int[] arrayInt) {
        //Если сумма его цифр делится на 3
        int summaryTotal = 0;
        for (int k = 0; k < arrayInt.length; k++) {
            summaryTotal = summaryTotal + Math.abs(arrayInt[k]);
        }
        for (int z = summaryTotal; summaryTotal >= 3; z--) {
            summaryTotal = summaryTotal - 3;
        }
        if (summaryTotal == 0) return true;
        else return false;
    }

    @Override
    public boolean verifyFour(int[] arrayInt) {
        if (arrayInt.length == 1 && (arrayInt[0] == 0 || Math.abs(arrayInt[0]) == 4 || Math.abs(arrayInt[0]) == 8)) return true;
        //Если число оканчивается на 00
        if (arrayInt.length > 2 && (arrayInt[arrayInt.length - 1] == 0 && arrayInt[arrayInt.length - 2] == 0))
            return true;
        //Если число оканчивается двумя цифрами, образующими число, делящееся на 4
        int summaryTotal = 0;
        if (arrayInt.length > 1)
            summaryTotal = Math.abs(arrayInt[arrayInt.length - 1] + arrayInt[arrayInt.length - 2] * 10);
        for (int z = summaryTotal; summaryTotal >= 4; z--) {
            summaryTotal = summaryTotal - 4;
        }
        if (arrayInt.length > 1 && summaryTotal == 0) return true;
        //Если сумма предпоследней цифры и половины последней цифры — чётное число
        float summaryLast = 0;
        if (arrayInt.length > 1)
            summaryLast = Math.abs((float) arrayInt[arrayInt.length - 1] / 2 + arrayInt[arrayInt.length - 2]);
        for (float z = summaryLast; summaryLast >= 2; z--) {
            summaryLast = summaryLast - 2;
        }
        if (arrayInt.length > 1 && summaryLast == 0) return true;
        else return false;
    }

    @Override
    public boolean verifyFive(int[] arrayInt) {
        //Если число оканчивается на 0, 5
        if (arrayInt[arrayInt.length - 1] == 0 || Math.abs(arrayInt[arrayInt.length - 1]) == 5) return true;
        else return false;
    }

    @Override
    public boolean verifyNine(int[] arrayInt) {
        //Если сумма его цифр делится на 9
        int summaryTotal = 0;
        for (int m = 0; m < arrayInt.length; m++) {
            summaryTotal = summaryTotal + Math.abs(arrayInt[m]);
        }
        for (int x = summaryTotal; summaryTotal >= 9; x--) {
            summaryTotal = summaryTotal - 9;
        }
        if ((arrayInt.length == 1 && Math.abs(arrayInt[0]) == 9) || summaryTotal == 0) return true;
        else return false;
    }

    @Override
    public boolean verifyEleven(int[] arrayInt) {
        //Если сумма цифр, стоящих на нечетных местах,
        //равна сумме цифр, стоящих на четных местах
        //или суммы должны отличаться на 11
        int summaryOfEven = 0;
        int summaryOfOdd = 0;
        for (int i = 0; i < arrayInt.length; i = i + 2) {
            summaryOfEven = summaryOfEven + Math.abs(arrayInt[i]);
        }
        for (int j = 1; j < arrayInt.length; j = j + 2) {
            summaryOfOdd = summaryOfOdd + Math.abs(arrayInt[j]);
        }
        if (summaryOfEven == summaryOfOdd || Math.abs(summaryOfEven - summaryOfOdd) == 11) return true;
        else return false;
    }
}
