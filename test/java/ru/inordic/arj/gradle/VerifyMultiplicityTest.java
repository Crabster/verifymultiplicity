package ru.inordic.arj.gradle;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class VerifyMultiplicityTest {

    @Test
    void verifyTwo() {
        VerifyMultiplicityLogic verifier = new VerifyMultiplicityLogic();
        Assertions.assertTrue(verifier.verifyTwo(new int[]{-8, -6}));
        Assertions.assertTrue(verifier.verifyTwo(new int[]{8, 6}));

        Assertions.assertFalse(verifier.verifyTwo(new int[]{1, 7}));
        Assertions.assertFalse(verifier.verifyTwo(new int[]{-1, -7}));
    }

    @Test
    void verifyThree() {
        VerifyMultiplicityLogic verifier = new VerifyMultiplicityLogic();
        Assertions.assertTrue(verifier.verifyThree(new int[]{1, 2, 9}));
        Assertions.assertTrue(verifier.verifyThree(new int[]{-1, -2, -9}));

        Assertions.assertFalse(verifier.verifyThree(new int[]{2, 3, 5}));
        Assertions.assertFalse(verifier.verifyThree(new int[]{-2, -3, -5}));
    }

    @Test
    void verifyFour() {
        VerifyMultiplicityLogic verifier = new VerifyMultiplicityLogic();
        Assertions.assertTrue(verifier.verifyFour(new int[]{1, 0, 8}));
        Assertions.assertTrue(verifier.verifyFour(new int[]{-1, 0, 8}));

        Assertions.assertFalse(verifier.verifyFour(new int[]{1, 1, 3}));
        Assertions.assertFalse(verifier.verifyFour(new int[]{-1, -1, -3}));
    }

    @Test
    void verifyFive() {
        VerifyMultiplicityLogic verifier = new VerifyMultiplicityLogic();
        Assertions.assertTrue(verifier.verifyFive(new int[]{1, 5, 0}));
        Assertions.assertTrue(verifier.verifyFive(new int[]{-1, -5, 0}));

        Assertions.assertFalse(verifier.verifyFive(new int[]{1, 6, 3}));
        Assertions.assertFalse(verifier.verifyFive(new int[]{-1, -6, -3}));
    }

    @Test
    void verifyNine() {
        VerifyMultiplicityLogic verifier = new VerifyMultiplicityLogic();
        Assertions.assertTrue(verifier.verifyNine(new int[]{9, 0}));
        Assertions.assertTrue(verifier.verifyNine(new int[]{-9, 0}));

        Assertions.assertFalse(verifier.verifyNine(new int[]{1, 3, 7}));
        Assertions.assertFalse(verifier.verifyNine(new int[]{-1, -3, -7}));
    }

    @Test
    void verifyEleven() {
        VerifyMultiplicityLogic verifier = new VerifyMultiplicityLogic();
        Assertions.assertTrue(verifier.verifyEleven(new int[]{1, 2, 1}));
        Assertions.assertTrue(verifier.verifyEleven(new int[]{-1, -2, -1}));

        Assertions.assertFalse(verifier.verifyEleven(new int[]{3, 5}));
        Assertions.assertFalse(verifier.verifyEleven(new int[]{-3, -5}));
    }
}